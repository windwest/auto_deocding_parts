#!/usr/bin/env python
# -*- coding: utf-8 -*-

# @Time    : 2020/9/15 下午5:11
# @Author  : Hanley
# @File    : service_util.py
# @Desc    : 

import time
import traceback
import ujson

from tornado.httpclient import AsyncHTTPClient

from commons.initlog import logging
from commons.status import *


def cost_time(func):
    async def wrapper(*args, **kwargs):
        start = time.time()
        return_data = await func(*args, **kwargs)
        end = time.time()
        used = end - start
        url = args[0].url
        logging.debug(f"===url: {url} cost time: {used}===")
        return return_data
    return wrapper

@cost_time
async def fetch_response(req):
    rpc_data = {"code": CODE_503, "msg": "外部接口调用异常", "time": time.time()}
    try:
        response = await AsyncHTTPClient().fetch(req)
        rpc_data = ujson.loads(response.body)
    except BaseException:
        if req.method == "GET":
            logging.error("route: {} return error".format(req.url))
        else:
            param = ujson.loads(req.body)
            logging.error("route: {}, param: {} return error".format(req.url, param))
        logging.error(traceback.format_exc())
    finally:
        return rpc_data
