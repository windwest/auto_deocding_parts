#!/usr/bin/env python
# -*- coding: utf-8 -*-

# @Time    : 2020/9/15 上午10:57
# @Author  : Hanley
# @File    : request_util.py
# @Desc    :
import json
import time
import traceback
from copy import deepcopy

import ujson
from tornado.httpclient import AsyncHTTPClient
from tornado.web import RequestHandler

from commons.common import Constant
from commons.constant import ReturnCodeMap
from commons.initlog import logging
from commons.status import *
from utils.database_util import MongodbConnect, RedisConnect, ESConnect


class BaseHandler(RequestHandler):

    def __init__(self, application, request, **kwargs):
        super(BaseHandler, self).__init__(application, request, **kwargs)

    @property
    def _redis(self):
        return RedisConnect().client

    @property
    def _mongo(self):
        return MongodbConnect().client

    @property
    def _es(self):
        return self.application._es_conn

    def prepare(self):
        self.simple_param()
        self.make_params()
        super().prepare()

    def make_params(self):
        self.getDomain()
        self.init_web_param()

    def init_web_param(self):
        domain = self.parameter["domain"]
        preFix = domain.split(".")[0]
        channelCookieKey = Constant.COOKIE_CHANNEL.format(preFix)
        deviceCookieKey = Constant.COOKIE_DEVICE.format(preFix)
        uidCookieKey = Constant.COOKIE_UID.format(preFix)
        uid = self.inner_check("uid", uidCookieKey)
        device = self.inner_check("device", deviceCookieKey)
        channel = self.inner_check("channel", channelCookieKey)
        self.parameter["uid"] = uid
        self.parameter["device"] = device
        self.parameter["channel"] = channel

    def getDomain(self):
        hostDomain = self.parameter.get("domain") if self.parameter.get("domain") \
            else self.request.headers._dict.get("Host")
        real_ip = self.request.headers.get("X-Real-IP")
        real_ip = real_ip if real_ip else self.request.remote_ip
        self.parameter["real_ip"] = real_ip
        self.parameter["domain"] = hostDomain

    def inner_check(self, key, cookieKey):
        if self.parameter.get(key):
            value = self.parameter.get(key)
        else:
            if self.get_secure_cookie(cookieKey):
                value = self.get_secure_cookie(cookieKey).decode()
            else:
                value = None
        return value

    def simple_param(self):
        self.parameter = {}
        query = deepcopy(self.request.arguments)
        for key in query:
            if isinstance(query[key], list):
                query[key] = query[key][0]
            if isinstance(query[key], bytes):
                query[key] = query[key].decode()
            self.parameter[key] = query[key]
        if self.request.body and self.request.headers.get(
                "Content-Type", "").startswith("application/json"):
            try:
                self.json_args = ujson.loads(self.request.body)
            except BaseException:
                logging.error("can't loads param: {}".format(
                    self.request.body))
                result = ReturnData(CODE_600)
                self.write(result.value)
                self.finish()
            else:
                self.parameter.update(self.json_args)

    async def fetch_res(self,req,language=""):
        """远程rpc接口异步调用"""

        rpc_data = {}
        try:
            # if not language:
            #     language = "zh"
            # # 统一为rpc 增加语言类型参数
            # if isinstance(req.headers,dict):
            #     req.headers["Sys-Language"] = language
            # else:
            #     req.headers.add("Sys-Language", language)
            response = await AsyncHTTPClient().fetch(req)
            rpc_data = json.loads(response.body)

        except Exception as ex:
            logging.error('req method:'+req.method)
            logging.error('req url:'+req.url)
            if req.body:
                if isinstance(req.body,bytes):
                    body = req.body.decode()
                else:
                    body = req.body
                logging.error('req body:' + body)
            logging.error(traceback.format_exc())
            except_args = ex.args
            except_type = type(ex).__name__
            template = "An exception of type {0} occurred. Arguments:\n{1!r}"
            message = template.format(except_type, except_args)
            logging.debug("-----------%s" % message)
            if "Timeout" in except_args[1]:
                exception_type = "timeout"
                exception_intro = "==从rpc获取数据超时"
            else:
                exception_type = "error"
                exception_intro = "==从rpc获取数据出错"
            rpc_data = {'code':0,"exception_type": exception_type,"exception_intro":exception_intro,'info':str(ex),'data':None}
        finally:
            return rpc_data


class ReturnData(object):

    def __init__(self, code=1, data=None, flag=True, msg=None, decimal=False, **kwargs):
        self.code = code
        self.message = msg if msg else ReturnCodeMap.CN_CODE[code]
        self.data = self.format_float(data) if decimal else data
        self.flag = flag
        self.kwargs = kwargs

    def for_dict(self, _dict: dict):
        for k in _dict:
            if isinstance(_dict[k], dict):
                self.format_float(_dict[k])
            else:
                if isinstance(_dict[k], float):
                    _dict[k] = "{:.2f}".format(round(_dict[k], 2))
                elif isinstance(_dict[k], list):
                    _dict[k] = type(_dict[k])([self.format_float(k) for k in _dict[k]])
                else:
                    continue
        return _dict

    def format_float(self, data):
        if isinstance(data, dict):
            return self.for_dict(data)
        if isinstance(data, float):
            return "{:.2f}".format(round(data, 2))
        if isinstance(data, list):
            return type(data)([self.format_float(k) for k in data])
        return data

    @property
    def value(self):
        returnMap = {
            "code": self.code,
            "msg": self.message,
            "time": time.time(),
        }
        if self.flag:
            returnMap.update({"data": self.data})
        if self.kwargs:
            returnMap.update(self.kwargs)
        return returnMap
