#!/usr/bin/env python
# -*- coding: utf-8 -*-

# @Time    : 2020/9/15 下午1:41
# @Author  : Hanley
# @File    : tasks.py
# @Desc    : 

from __future__ import absolute_import, unicode_literals

from celery import Task

from commons.initlog import logging
from .app import app


class MyTask(Task):

    def on_success(self, retval, task_id, args, kwargs):
        return super(MyTask, self).on_success(retval, task_id, args, kwargs)

    def on_failure(self, exc, task_id, args, kwargs, einfo):
        logging.debug('task fail, reason: {0}, task_id - {1}, '
                      'args - {2}, kwargs - {3}, einfo - {4}'.format(
            exc, task_id, args, kwargs, einfo))
        return super(MyTask, self).on_failure(exc, task_id, args, kwargs, einfo)


@app.task(base=MyTask)
def monitor():
    logging.debug(f"Enter monitor")
    logging.debug(f"End monitor")
    return
