#!/usr/bin/env python
# -*- coding: utf-8 -*-

# @Time    : 2020/9/15 下午1:40
# @Author  : Hanley
# @File    : app.py
# @Desc    : 

from __future__ import absolute_import, unicode_literals
from celery import Celery

app = Celery('async_task')

app.config_from_object('async_task.config')

if __name__ == '__main__':
    app.start()
