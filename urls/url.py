#!/usr/bin/env python
# -*- coding: utf-8 -*-

# @Time    : 2020/9/15 下午2:13
# @Author  : Hanley
# @File    : url.py
# @Desc    : 

from urls import error
from urls import (
    decoding_parts,
    search_key
)


def handlers_loads():

    handlers = []

    # handlers.extend(user.router)
    handlers.extend(decoding_parts.router)
    handlers.extend(search_key.router)
    handlers.extend(error.router)
    return handlers
