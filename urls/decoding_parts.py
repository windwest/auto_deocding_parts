#coding: utf-8

from tornado.web import url

from handlers import decoding_parts_handler


router = [

    url(r'/auto_decoding_parts', decoding_parts_handler.AutoDecodingParts, name="自动译码"),
    url(r'/auto_decoding_parts_new', decoding_parts_handler.NewAutoDecodingParts, name="自动译码带替换件"),
    url(r'/auto_decoding_parts_replacement', decoding_parts_handler.AutoDecodingPartsReplacement, name="自动译码带替换件最新零件返回"),
    url(r'/auto_decoding_parts_ugc', decoding_parts_handler.AutoDecodingPartsMultiDataSource, name="自动译码使用用户数据的接口")

]
