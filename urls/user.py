#!/usr/bin/env python
# -*- coding: utf-8 -*-

# @Time    : 2020/9/15 下午2:13
# @Author  : Hanley
# @File    : user.py
# @Desc    : 

from tornado.web import url
from handlers import user


router = [

    url(r'/api/user/login', user.UserLogin, name="用户登陆"),

]
