#!/usr/bin/env python
# -*- coding: utf-8 -*-

# @Time    : 2020/9/15 下午2:16
# @Author  : Hanley
# @File    : error.py
# @Desc    : 

from tornado.web import url

from handlers import error

router = [
    url(r'(.*)', error.ErrorHandler, name="未知请求")
]