#!/usr/bin/env python
# -*- coding: utf-8 -*-

# @Time    : 2020/9/15 下午2:01
# @Author  : Hanley
# @File    : base.py
# @Desc    :

import time
import datetime

from peewee import (
    CharField,
    DateTimeField,
    IntegerField,
    Model,
    TextField,
    ForeignKeyField,
    SmallIntegerField,
    DecimalField
)

from utils.database_util import RetryConnectMysql

_mysql = RetryConnectMysql.connect_mysql()


class BaseModel(Model):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    class Meta:
        database = _mysql


class CommonBase(BaseModel):

    status = SmallIntegerField(null=False, default=1, index=True, verbose_name="用户状态")
    create_time = CharField(max_length=64, default=time.strftime("%Y-%m-%d %X"),
                            verbose_name="创建时间")
    update_time = CharField(max_length=64, default=time.strftime("%Y-%m-%d %X"),
                            verbose_name="更新时间")


class UserBase(BaseModel):

    uid = CharField(null=False, max_length=64, unique=True, verbose_name="用户唯一id")
    mobile = CharField(null=False, max_length=32, unique=True, verbose_name="登陆账号")
    status = SmallIntegerField(null=False, default=1, index=True, verbose_name="用户状态")
    role_id = IntegerField(null=False, default=9, index=True, verbose_name="用户角色")
    device = CharField(max_length=16, null=False, default="android",
                       index=True, verbose_name="设备名称")

    username = CharField(null=False, default="", max_length=32, verbose_name="用户姓名")
    password = CharField(null=False, max_length=64, verbose_name="用户密码")
    last_login_ip = CharField(null=True, max_length=32, verbose_name="登陆ip")
    gender = SmallIntegerField(null=False, default=1, verbose_name="性别")
    company = CharField(null=False, default="", max_length=64, verbose_name="公司名称")
    registration_time = CharField(max_length=64, default=time.strftime("%Y-%m-%d %X"),
                                  verbose_name="注册时间")
    last_login_time = CharField(max_length=64, default=time.strftime("%Y-%m-%d %X"),
                                verbose_name="登陆时间")
