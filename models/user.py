#!/usr/bin/env python
# -*- coding: utf-8 -*-

# @Time    : 2020/9/15 下午2:04
# @Author  : Hanley
# @File    : user.py
# @Desc    : 

from models.base import _mysql, UserBase


class User(UserBase):

    class Meta:
        verbose_name = "用户表"
        table_name = "user"


if __name__ == '__main__':
    list_model = [User]
    list_model = [item for item in list_model if not item.table_exists()]
    print("Start create models: " + ",".join([item.__name__ for item in list_model]))
    _mysql.create_tables(list_model)
    print("End create models successful")
