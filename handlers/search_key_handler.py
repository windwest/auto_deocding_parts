#coding: utf-8
import json
from copy import deepcopy
from operator import itemgetter
from itertools import groupby
from commons.status import *
from commons.wrapper import no_parameter_check
from utils.request_util import BaseHandler, ReturnData
from commons.initlog import logging
from commons.common import sdk_info
from _dc_config import brand_db_map
from removeship import remove_ship_brand
import time
import re

html_re = re.compile(r'<.*?b.*?>')


class PidPartsSearch(BaseHandler):

    @no_parameter_check()
    async def get(self):
        # 获取参数
        vin = self.parameter.get("vin", "")
        vin_extra_info = self.parameter.get("vin_extra_info", '{}')
        if isinstance(vin_extra_info, str):
            vin_extra_info = json.loads(vin_extra_info)
        query = self.parameter.get("query", "")
        brandCode = self.parameter.get("brandCode", "")
        cid = self.parameter.get("cid", "")
        yc_id = self.parameter.get("yc_id", "")

        # 处理查询关键词
        query_type = self.check_query_str_type(query)
        query = self.label_process(query)

        # 通过brandCode获取 数据库名
        db_name = brand_db_map.get(brandCode, "")

        position_list = []

        # query为pid时的查询
        if query_type == "pid":
            pid_list = self.get_pid_list_with_es(brandCode, cid, query, db_name)
            part_info_list = self.query_part_detail_info(brandCode, cid, pid_list, status='pid')
            stdlabel = ""

        # query为名称时的查询
        else:
            # 算法解析名称获取 标准名称及位置
            alysis_list = sdk_info.match(query)

            stdlabel, position = alysis_list[0], alysis_list[1]
            query_score = alysis_list[2]

            # 译码算法得分低于分数值后直接查询原厂名称
            if query_score < 0.84:
                part_info_list = []

            else:
                if stdlabel:
                    # 处理位置信息
                    if position:
                        position_list = self.positionSplit(position)
                    else:
                        position_list = []

                    part_info_list = self.query_part_detail_info(brandCode, cid, stdlabel, status='stdname')

                else:
                    part_info_list = []

        if not part_info_list:
            pid_list = self.get_pid_list_with_es(brandCode, cid, query, db_name, status="2")
            part_info_list = self.query_part_detail_info(brandCode, cid, pid_list, status='pid') if pid_list else []

        logging.info(f"车架号 {vin}, cid {cid}, query {query}, 过滤前零件数量为 {len(part_info_list)}")

        # 兼容车架号和车型下的搜索(如果有车架号和车架号过滤信息则需要过滤零件)
        if vin and vin_extra_info:
            getPartList = []
            if "," in cid:
                for key, group in groupby(sorted(part_info_list, key=itemgetter('cid')), itemgetter('cid')):
                    group = list(group)
                    for k, v in groupby(sorted(group, key=itemgetter('mid')), itemgetter('mid')):
                        getPartList.extend(remove_ship_brand(brandCode, list(v), vin_extra_info, vin, ctype="maintenance", _cid=key))

            else:
                for key, group in groupby(sorted(part_info_list, key=itemgetter('mid')), itemgetter('mid')):
                    getPartList.extend(remove_ship_brand(brandCode, list(group), vin_extra_info, vin, ctype="maintenance", _cid=cid))

        else:
            getPartList = part_info_list

        # logging.info(f'过滤后零件列表{getPartList}')

        result_list = self.checkPosition(brandCode, getPartList, position_list)

        if not result_list and stdlabel == "控制臂":
            result_list = self.update_pid_info_price(brandCode, getPartList)
            # result_list = getPartList

        logging.info(f"车架号 {vin}, cid {cid}, query {query}, 返回零件数量为 {len(result_list)}")

        return ReturnData(CODE_1, result_list)

    async def post(self):
        await self.get()

    def get_part_price_image(self, brandCode, pids):
        PART_IMG_PATH = "https://phyimgs.007vin.com"
        args = {'brandCode': brandCode, 'pid': {'$in': pids}}
        res = {'_id': 0}
        part_list = self._mongo['price']['total_part_information'].find(args, res)
        rd = {}
        for part in part_list:
            sale_price = '暂无价格'
            img = ''
            if 'images' in part and part['images']:
                images = part.pop('images')
                img = PART_IMG_PATH + '/' + images[0] if images[0] else ''
            part['img'] = img
            if 'price' in part:
                pid_price = part['price']
                pid_price_map = {pc['firm_code']: pc for pc in pid_price if pc.get('firm_code')}
                """
                获取价格规则
                audi -> firm_code==audi
                vwag -> 优先firm_code in (svw\cvw)，没有svw cvw就随便取一个
                skoda -> sk==firm_code
                """
                sale_price = '暂无价格'
                pid_prices, _price = pid_price_map, pid_price
                if brandCode == 'vwag':
                    if 'svw' in pid_prices.keys():
                        sale_price = "%.2f" % (float(pid_prices.get('svw').get('price')) / 100)
                    elif 'cvw' in pid_prices:
                        sale_price = "%.2f" % (float(pid_prices.get('cvw').get('price')) / 100)
                    else:
                        if len(_price) > 0:
                            sale_price = "%.2f" % (float(_price[0].get('price')) / 100)
                elif brandCode == 'skoda':
                    if 'sk' in pid_prices.keys():
                        sale_price = "%.2f" % (float(pid_prices.get('sk').get('price')) / 100)
                    else:
                        if len(_price) > 0:
                            sale_price = "%.2f" % (float(_price[0].get('price')) / 100)
                else:
                    if brandCode in pid_prices:
                        sale_price = "%.2f" % (float(pid_prices.get(brandCode).get('price')) / 100)
                    else:
                        if len(_price) > 0:
                            sale_price = "%.2f" % (float(_price[0].get('price')) / 100)
            re_keys = ['label', 'remark', 'quantity']
            for k in re_keys:
                part[k] = html_re.sub('', part.get(k) or '')
            part['price'] = sale_price
            rd[part['pid']] = part
        return rd

    # 根据位置信息过滤出零件并检查颜色件(优先返回颜色件)
    def checkPosition(self, brandCode, parts_list, position_list):
        result_list = []
        all_pid_list = []

        for part_info in parts_list:
            if part_info.get('colorvalue', 1) == 0:
                continue
            standard_label_remark = part_info.get('standard_label_remark', "").replace(" ", "")
            standard_label_remark_list = standard_label_remark.split(",") if standard_label_remark else []

            pid = part_info.get("pid")
            if not standard_label_remark_list or (set(standard_label_remark_list) >= set(position_list)):
                if pid not in all_pid_list:
                    result_list.append(part_info)
                    all_pid_list.append(pid)

        colorparts_list = []
        for data in result_list:
            pid, parentpid = data.get("pid"), data.get("parentpid", "")
            if parentpid in all_pid_list:
                colorparts_list.append(data)

        if colorparts_list:
            r_list = colorparts_list
        else:
            r_list = result_list

        r_list = self.update_pid_info_price(brandCode, r_list)

        return r_list

    def update_pid_info_price(self, brandCode, pid_info_list):
        pid_list = [i.get("pid") for i in pid_info_list]
        priceDict = self.get_part_price_image(brandCode, pid_list)
        for data in pid_info_list:
            data['maingroupname'] = data['maingroup_label']
            data['subgroupname'] = data['subgroup_label']
            data['name'] = deepcopy(data['label'])
            data['url'] = "https://structimgs.007vin.com/" + data['imagePath'].replace('struct', 'thumbnail')
            data['price'] = priceDict.get(data['pid'])['price']

        return pid_info_list

    # 通过es查询获取pid列表
    def get_pid_list_with_es(self, brandCode, cid, query, db_name, **kwargs):
        base_es_query_dict = self.base_data_es_query(query, cid, brandCode, status=kwargs.get("status", "1"))
        base_es_res = self._es.search(index=f"base_data_{db_name}", body=base_es_query_dict)
        pid_list = [es_data.get("_source", {}).get("pid", "") for es_data in base_es_res.get("hits", {}).get("hits", [])]

        return pid_list

    def query_part_detail_info(self, brandCode, cid, q, **kwargs):
        """
            查询零件详细信息
        """
        db_name = brand_db_map[brandCode]
        if "," in cid:
            cid_list = cid.split(",")
            cond = {"cid": {"$in": cid_list}, "brandCode": brandCode}
        else:
            cond = {"cid": cid, 'brandCode': brandCode}

        if kwargs['status'] == "pid":
            cond['pid'] = {"$in": q}
        else:
            cond['standard_label'] = q

        parts_list = list(self._mongo[db_name]['pid_filter_info_collection'].find(cond, {"_id": 0}))

        return parts_list

    def positionSplit(self, position):
        if "左" in position and '右' in position:
            position = position.replace("左", "").replace("右", "")
        if "前" in position and '后' in position:
            position = position.replace("前", "").replace("后", "")
        if "内" in position and '外' in position:
            position = position.replace("内", "").replace("外", "")

        return list(position)

    def base_data_es_query(self, keyword, cid, brandCode, size=200, status="1"):
        cid_list = cid.split(",")
        query_dict = {
            "size": size,
            "query": {
                "bool": {
                    "must": [
                        {"match": {"keywords_list.py": keyword}}
                    ],
                    "filter": [
                        {"term": {"brandCode": brandCode}},
                        {"terms": {"cid.keyword": cid_list}}
                    ]
                }
            },
            "sort": {
                "weight": {
                    "order": "desc"
                }
            }
        }

        if status == '1' and brandCode == "benz":
            query_dict['query']["bool"]['must'][0]['match'] = {"pid.py": keyword}

        if status == '2':
            query_dict['query']["bool"]['must'][0]['match'] = {"keywords_list2.py": keyword}
        return query_dict

    def delete_symbols(self, string):
        temp_list = []
        for letter in string:
            if u'\u4e00' <= letter <= u'\u9fff' or letter.isalpha() or letter.isdigit():
                temp_list.append(letter)
        return "".join(temp_list)

    def label_process(self, label):
        label = label.replace("\t", "").replace('"', "")
        label = label.replace("<br/>", "")
        label = self.delete_symbols(label)
        label = label.replace("，", "")
        label = label.upper()
        return label

    def check_query_str_type(self, query_str):
        query_type = "pid"
        if self.check_chinese(query_str):
            query_type = "chinese"

        return query_type

    def check_chinese(self, check_str):
        for ch in check_str:
            if u'\u4e00' <= ch <= u'\u9fff':
                return True
        return False