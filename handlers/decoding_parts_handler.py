#coding: utf-8
import datetime
import re
import traceback
import json
import requests
from collections import Counter
from operator import itemgetter
from itertools import groupby
from commons.status import *
from commons.wrapper import no_parameter_check
from utils.request_util import BaseHandler, ReturnData
from commons.initlog import logging
from commons.common import sdk_info
from commons.common import zk_label_exists
from tornado.httpclient import HTTPRequest
from tornado.httputil import url_concat
from commons.brands_static_vol import land_rover_remachine_pid_list
from commons.brands_static_vol import land_rover_oil_detail_info
from _dc_config import brand_db_map
from removeship import remove_ship_brand
import time


class AutoDecodingParts(BaseHandler):

    async def get(self):
        cid = self.parameter.get("cid", "")
        vin = self.parameter.get("vin", "")
        vin_extra_info = self.parameter.get("vin_extra_info", '{}')
        if isinstance(vin_extra_info, str):
            vin_extra_info = json.loads(vin_extra_info)
        brandCode = self.parameter.get("brandCode", "")
        query_str = self.parameter.get("query_str", "")
        if not brandCode or brandCode not in brand_db_map:
            return_data = ReturnData(CODE_600, [])

        query_str = self.filter_str(query_str)

        alias_es_query = self.alias_search_standard_label(query_str)

        es_query_alias_result = self._es.search(index="auto_decoding_parts", body=alias_es_query)

        alias_res_list = es_query_alias_result.get("hits", {}).get("hits", [])

        if alias_res_list:

            standard_label_list = []

            position_list = []

            for data in alias_res_list:
                standard_label = data.get("_source", {}).get("standard_label", "")
                alias_name = data.get("_source", {}).get("alias_name", "")
                position = data.get("_source", {}).get("position", "").replace(" ", "")
                if standard_label not in standard_label_list:
                    standard_label_list.append(standard_label)

                if position:
                    p_list = position.split(",")
                    for p in p_list:
                        if p not in position_list:
                            position_list.append(p)

            logging.info(f'-----{standard_label_list}-----')

            if len(standard_label_list) > 1:
                return_data = ReturnData(CODE_0, [])

            else:
                parts_list = self.query_stdname_extra_info_with_cid(brandCode, cid, standard_label_list[0])
                getPartList = remove_ship_brand(brandCode, parts_list, vin_extra_info, vin, ctype="maintenance", _cid=cid)
                check_position_list = self.checkPosition(getPartList, position_list)
                return_data = ReturnData(CODE_1, check_position_list)

        else:
            return_data = ReturnData(CODE_1, [])

        self.write(return_data.value)
        self.finish()
        return

    async def post(self):
        await self.get()

    # 根据位置信息过滤出零件并检查颜色件(优先返回颜色件)
    def checkPosition(self, parts_list, position_list):
        result_list = []

        for part_info in parts_list:
            if part_info.get('colorvalue', 1) == 0:
                continue
            standard_label_remark = part_info.get('standard_label_remark').replace(" ", "")
            standard_label_remark_list = standard_label_remark.split(",")
            if set(standard_label_remark_list) >= set(position_list):
                result_list.append(part_info)

        colorparts_list = []
        all_pid_list = [i.get("pid") for i in result_list]
        for data in result_list:
            pid, parentpid = data.get("pid"), data.get("parentpid", "")
            if parentpid in all_pid_list:
                colorparts_list.append(data)

        if colorparts_list:
            return colorparts_list

        else:
            return result_list

    # 获取pid_filter_info_colleciton中的零件信息
    def query_stdname_extra_info_with_cid(self, brandCode, cid, stdname):
        """
            通过标准名称查询零件
        """
        db_name = brand_db_map[brandCode]
        if "," in cid:
            cid_list = cid.split(",")
            cond = {"cid": {"$in": cid_list}, "brandCode": brandCode}
        else:
            cond = {"cid": cid, 'brandCode': brandCode}

        cond['standard_label'] = stdname

        parts_list = list(self._mongo[db_name]['pid_filter_info_collection'].find(cond,
                                                                                  {"_id": 0,
                                                                                   'createtime': 0}))

        return parts_list

    # 通过别名的名称搜索标准名称
    def alias_search_standard_label(self, query, size=200):
        query_dict = {
            "size": size,
            "query": {
                "bool": {
                    "must": [
                        {"term": {"alias_name": query}}
                    ]
                }
            }
        }

        return query_dict

    # 处理查询关键字中的特殊字符
    def filter_str(self, desstr, restr=''):
        res = re.compile("[^\\u4e00-\\u9fa5^a-z^A-Z]")
        f_str = res.sub(restr, desstr)
        f_str_upper = f_str.upper()
        if re.findall(r'[A-Z]', f_str):
            status = False
            for keyword in ['LED', 'EGR', 'HVAC', 'ABS', 'L', 'R', 'ACC', 'A柱', 'B柱', 'C柱', 'D柱']:
                if keyword in f_str_upper:
                    status = True

            if status:
                return f_str
            else:
                return re.sub(r'[a-zA-z]', '', f_str)

        else:
            return f_str


class NewAutoDecodingParts(BaseHandler):

    @no_parameter_check()
    async def get(self):
        cid = self.parameter.get("cid", "")
        vin = self.parameter.get("vin", "")
        vin_extra_info = self.parameter.get("vin_extra_info", '{}')
        if isinstance(vin_extra_info, str):
            vin_extra_info = json.loads(vin_extra_info)
        brandCode = self.parameter.get("brandCode", "")
        query_str = self.parameter.get("query_str", "")
        yc_id = self.parameter.get("yc_id", "")
        merchant_id = self.parameter.get("merchant_id", "")
        is_log = self.parameter.get("is_log", "yes")

        logging.info(f"车架号=>{vin}, brandCode=>{brandCode}, cid=>{cid}, 译码名=>{query_str}, 译码员=>{yc_id}")

        decode_log = {
            "path": "auto_decoding_parts_new",
            "brandCode": brandCode,
            "vin": vin,
            "cid": cid,
            "query": query_str,
            "stdlabel": "",
            "pids": "",
            "is_show": "",
            "pid_length": "",
            "yc_id": yc_id,
            "create_time": datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        }

        std_black_list = [stdlabel for data in self._mongo['price']['auto_decoding_black_list'].find({"brandCode": {"$in": ["all", brandCode]}}) for stdlabel in data.get("black_stdlabel_list")]

        query_str = self.label_process(query_str)

        try:
            alysis_list = sdk_info.match(query_str)
        except:
            alysis_list = ["", ""]

        stdlabel, position = alysis_list[0], alysis_list[1]

        if stdlabel:
            stdlabel, position = self.specail_pos_info_handler(stdlabel, position, query_str)

        else:
            decode_log['decode_msg'] = "未译码成功"
            if is_log == "yes":
                self._redis.lpush("auto_decoding_parts_log", json.dumps(decode_log))

            return ReturnData(CODE_600, [])

        if not brandCode or brandCode not in brand_db_map or not stdlabel:
            check_position_list = []
            return ReturnData(CODE_600, check_position_list)

        else:
            stdlabel = self.filter_str(stdlabel)

            if stdlabel in std_black_list:
                return ReturnData(CODE_0, [])

            if position:
                position_list = self.positionSplit(position)
            else:
                position_list = []

            parts_list = self.query_stdname_extra_info_with_cid(brandCode, cid, stdlabel)

            getPartList = []
            if "," in cid:
                for key, group in groupby(sorted(parts_list, key=itemgetter('cid')), itemgetter('cid')):
                    group = list(group)
                    for k, v in groupby(sorted(group, key=itemgetter('mid')), itemgetter('mid')):
                        getPartList.extend(remove_ship_brand(brandCode, list(v), vin_extra_info, vin, ctype="maintenance", _cid=key))

            else:
                for key, group in groupby(sorted(parts_list, key=itemgetter('mid')), itemgetter('mid')):
                    getPartList.extend(remove_ship_brand(brandCode, list(group), vin_extra_info, vin, ctype="maintenance", _cid=cid))

            colorvalue_list = {i.get("colorvalue") for i in getPartList if i.get('colorvalue') == 1}

            if not colorvalue_list and brandCode in ('audi', 'vwag', 'seat', 'skoda') and stdlabel in ('点火线圈', ' 火花塞', ' 保险杠', ' 减震器', ' 半轴', ' 大灯', ' 活性炭罐', ' 氧传感器', ' 外后视镜'):
                getPartList = remove_ship_brand(brandCode, parts_list, vin_extra_info, vin, ctype="parts", _cid=cid)

            check_position_list = self.checkPosition(getPartList, position_list)

            if merchant_id != '3':
                check_position_list = self.brandManager(brandCode, check_position_list, position)

            is_show_pid = self.show_all_partinfo_status(check_position_list, stdlabel)

            if brandCode in ('audi', 'vwag', 'seat', 'skoda') and check_position_list and is_show_pid == 0:
                second_ship_list = [info for info in remove_ship_brand(brandCode, check_position_list, vin_extra_info, vin, ctype="strict", _cid=cid) if info.get("colorvalue", 1) == 1]
                if len(second_ship_list) == 1:
                    check_position_list = second_ship_list
                    is_show_pid = 1

            pids = ",".join([i.get("pid") for i in check_position_list]) if check_position_list else ""

            pid_length = len(check_position_list)

            logging.info(f"车架号=>{vin}, brandCode=>{brandCode}, cid=>{cid}, 标准名称=>{stdlabel}, 译码零件数量=>{pid_length}")

            decode_log.update({
                "stdlabel": stdlabel,
                "pids": pids,
                "is_show": is_show_pid,
                "pid_length": pid_length
            })
            if is_log == "yes":
                self._redis.lpush("auto_decoding_parts_log", json.dumps(decode_log))

            if merchant_id == '3':
                if pid_length == 1:
                    return ReturnData(CODE_1, check_position_list, show_pid=1)
                else:
                    return ReturnData(CODE_1, check_position_list, show_pid=0)

            else:
                return ReturnData(CODE_1, check_position_list, show_pid=is_show_pid)

    async def post(self):
        await self.get()

    # 是否将最后零件列表结果全部展示
    def show_all_partinfo_status(self, parts_list, stdlabel):
        position_weight_dict = {
            "前": 1,
            "后": 2,
            "左": 3,
            "右": 4,
            "内": 5,
            "外": 6,
            "上": 7,
            "下": 8,
            "中": 9
        }
        show_pidinfo_list = ['火花塞', '传动皮带']
        if len(parts_list) == 1:
            return 1

        if stdlabel in show_pidinfo_list:
            return 1

        remark_count_dict = {}
        for part in parts_list:
            standard_label_remark = part.get("standard_label_remark", "")
            if not standard_label_remark:
                standard_label_remark = ""
            else:
                standard_label_remark = ",".join(sorted(standard_label_remark.replace(" ", "").split(","), key=lambda x: position_weight_dict.get(x, 10)))

            if standard_label_remark in remark_count_dict:
                remark_count_dict[standard_label_remark] += 1
            else:
                remark_count_dict[standard_label_remark] = 1

        for remark_count in remark_count_dict.values():
            if remark_count > 1:
                return 0

        return 1

    # 针对于特殊的零件
    def specail_pos_info_handler(self, stdlabel, position, query_str):
        if stdlabel == "气门":
            if "进" in query_str:
                position = "进"
            elif "排" in query_str:
                position = "排"

        if stdlabel == "蓄电池":
            if "辅助" in query_str:
                position = "辅助"

        if stdlabel == "翼子板":
            if "后" in query_str:
                stdlabel = "侧车架"

        if stdlabel == '车标':
            if "中网" in query_str:
                position = "前"
            elif "举升门" in query_str or "行李箱盖" in query_str:
                position = "后"

        if stdlabel in ('前舱盖', '行李箱盖'):
            if "前" in query_str:
                stdlabel = "前舱盖"

        if stdlabel == "安全气囊":
            if "副驾驶" in query_str:
                position = "前右"

        if stdlabel == "挡风玻璃":
            if "后" in query_str:
                stdlabel = '后窗玻璃'

        return stdlabel, position

    # 宝马品牌的特殊规则处理
    def bmw_series_handler(self, parts_list):
        if len(parts_list) == 1:
            return parts_list

        result_list = []
        for part_info in parts_list:
            label, standard_label = part_info.get("label", ""), part_info.get("standard_label", "")
            extra_info = part_info.get("extra_info")
            extra_info = json.loads(extra_info)
            ae_status = extra_info.get("AE", "")
            itid = part_info.get("itid", "")
            num = part_info.get("num", "")
            if ae_status == "AE" or itid == '--' or num == "03" or ("组件" in label and standard_label == '制动片'):
                continue
            result_list.append(part_info)

        if result_list:
            return result_list
        else:
            return parts_list

    # 奥迪品牌的特殊规则处理
    def audi_series_handler(self, parts_list):
        if len(parts_list) == 1:
            return parts_list

        result_list = []
        for part_info in parts_list:
            remark1 = part_info.get("remark1", "")
            condition = part_info.get("condition", "")
            if remark1:
                part_info['remark'] = remark1
            if remark1 == '1升':
                result_list.append(part_info)
            # if condition == 'L':
            #     result_list.append(part_info)

        if result_list:
            return result_list
        else:
            return parts_list

    # 路虎品牌的特殊规则处理
    def land_rover_series_handler(self, parts_list):
        if len(parts_list) == 1:
            return parts_list

        result_list = []
        for part_info in parts_list:
            pid = part_info.get("pid")
            standard_label = part_info.get("standard_label")

            if standard_label in ('变速箱油', '防冻液'):
                if pid in land_rover_oil_detail_info:
                    oil_info = land_rover_oil_detail_info[pid]
                else:
                    oil_info = ""

                if oil_info == '1L':
                    result_list.append(part_info)

            else:
                struct_extra_info = json.loads(part_info.get("struct_extra_info", "{}"))
                extra_info = json.loads(part_info.get("extra_info", "{}"))
                all_code_list = []
                if extra_info.get("filters", []):
                    for d in extra_info.get("filters", []):
                        all_code_list.extend(d.get("code", []))

                group_extra_info = struct_extra_info.get("group_extra_info", {})
                if group_extra_info and group_extra_info.get("filters", []):
                    for d in group_extra_info.get("filters", []):
                        all_code_list.extend(d.get("code", []))

                if struct_extra_info.get("filters", []):
                    for d in struct_extra_info.get("filters", []):
                        all_code_list.extend(d.get("code", []))

                if (21340 in all_code_list) or (19811 in all_code_list):
                    continue

                if part_info.get("pid", "") in land_rover_remachine_pid_list:
                    continue

                result_list.append(part_info)

        if result_list:
            return result_list
        else:
            return parts_list

    # 处理各个品牌相同规则过滤
    def all_series_handler(self, parts_list, position):
        if len(parts_list) == 1:
            return parts_list

        result_list = []
        for part_info in parts_list:
            standard_label, standard_label_remark = part_info.get("standard_label", ""), part_info.get("standard_label_remark", "")
            subgroup_label = part_info.get("subgroup_label", "")

            label, remark = part_info.get("label", ""), part_info.get("remark", "")
            verify_str = label + '##' + remark
            if standard_label == "蓄电池":
                if standard_label_remark == "辅助" and position == "辅助":
                    result_list.append(part_info)
                elif standard_label_remark == "辅助" and position == "":
                    continue
                elif standard_label_remark == "" and position == "":
                    result_list.append(part_info)

            elif "弃用零件" not in verify_str or "弃用" not in verify_str:
                result_list.append(part_info)

            elif standard_label == "轮辋" and "备用" not in subgroup_label:
                result_list.append(part_info)

        if result_list:
            return result_list
        else:
            return parts_list

    # 根据不同的品牌处理零件(新增规则)
    def brandManager(self, brandCode, parts_list, position):

        parts_list = self.all_series_handler(parts_list, position)
        if len(parts_list) == 1:
            return parts_list

        if brandCode in ('audi', 'vwag', 'seat', 'skoda'):
            return self.audi_series_handler(parts_list)

        elif brandCode == 'bmw':
            return self.bmw_series_handler(parts_list)

        elif brandCode == 'land_rover':
            return self.land_rover_series_handler(parts_list)

        else:
            return parts_list

    def positionSplit(self, position):
        if "左" in position and '右' in position:
            position = position.replace("左", "").replace("右", "")
        if "前" in position and '后' in position:
            position = position.replace("前", "").replace("后", "")

        if position == "辅助":
            return ['辅助']
        else:
            return list(position)

    # 根据位置信息过滤出零件并检查颜色件(优先返回颜色件)
    def checkPosition(self, parts_list, position_list):
        result_list = []
        all_pid_list = []
        all_replacement_list = []

        for part_info in parts_list:
            if part_info.get('colorvalue', 1) == 0:
                continue
            standard_label_remark = part_info.get('standard_label_remark').replace(" ", "")
            standard_label_remark_list = standard_label_remark.split(",")
            pid = part_info.get("pid")
            if not standard_label_remark or set(standard_label_remark_list) >= set(position_list):
                if pid in all_pid_list:
                    continue

                pid_replacement = part_info.get("pid_replacement", [])
                if pid_replacement and pid in pid_replacement:
                    pid_replacement.remove(pid)
                    all_replacement_list.extend(pid_replacement)

                result_list.append(part_info)
                all_pid_list.append(pid)

        if all_replacement_list:
            replace_status = 0
            for pid_info in result_list:
                if pid_info.get("pid") in all_replacement_list:
                    replace_status += 1

            if len(result_list) == replace_status:
                result_list = [result_list[-1]]

        # logging.info("result_list: %s", result_list)
        colorparts_list = []
        for data in result_list:
            pid, parentpid = data.get("pid"), data.get("parentpid", "")
            if parentpid in all_pid_list:
                colorparts_list.append(data)

        if colorparts_list:
            return colorparts_list

        else:
            return result_list

    # 获取pid_filter_info_colleciton中的零件信息
    def query_stdname_extra_info_with_cid(self, brandCode, cid, stdname):
        """
            通过标准名称查询零件
        """
        db_name = brand_db_map[brandCode]
        if "," in cid:
            cid_list = cid.split(",")
            cond = {"cid": {"$in": cid_list}, "brandCode": brandCode}
        else:
            cond = {"cid": cid, 'brandCode': brandCode}

        cond['standard_label'] = stdname

        parts_list = list(self._mongo[db_name]['pid_filter_info_collection'].find(cond,
                                                                                  {'_id': 0,
                                                                                   'cid': 1,
                                                                                   'mid': 1,
                                                                                   'num': 1,
                                                                                   'imagePath': 1,
                                                                                   'pid': 1,
                                                                                   'pnum': 1,
                                                                                   'remark1': 1,
                                                                                   'brandCode': 1,
                                                                                   'subgroup': 1,
                                                                                   'struct_extra_info': 1,
                                                                                   'standard_label_remark': 1,
                                                                                   'extra_info': 1,
                                                                                   'remark': 1,
                                                                                   'condition': 1,
                                                                                   'struct_label': 1,
                                                                                   'itid': 1,
                                                                                   'label': 1,
                                                                                   'model': 1,
                                                                                   'standard_label': 1,
                                                                                   'quantity': 1,
                                                                                   'parentpid': 1,
                                                                                   'pid_replacement': 1}))

        return parts_list

    # 通过别名的名称搜索标准名称
    def alias_search_standard_label(self, query, size=200):
        query_dict = {
            "size": size,
            "query": {
                "bool": {
                    "must": [
                        {"term": {"alias_name": query}}
                    ]
                }
            }
        }

        return query_dict

    def delete_symbols(self, string):
        temp_list = []
        for letter in string:
            if u'\u4e00' <= letter <= u'\u9fff' or letter.isalpha() or letter.isdigit():
                temp_list.append(letter)
        return "".join(temp_list)

    def label_process(self, label):
        label = label.replace("\t", "").replace('"', "")
        label = label.replace("<br/>", "")
        label = self.delete_symbols(label)
        label = label.replace("，", "")
        label = label.upper()
        return label

    # 处理查询关键字中的特殊字符
    def filter_str(self, desstr, restr=''):
        res = re.compile("[^\\u4e00-\\u9fa5^a-z^A-Z]")
        f_str = res.sub(restr, desstr)
        f_str_upper = f_str.upper()
        if re.findall(r'[A-Z]', f_str):
            status = False
            for keyword in ['LED', 'EGR', 'HVAC', 'ABS', 'L', 'R', 'ACC', 'A柱', 'B柱', 'C柱', 'D柱']:
                if keyword in f_str_upper:
                    status = True

            if status:
                return f_str
            else:
                return re.sub(r'[a-zA-z]', '', f_str)

        else:
            return f_str


class AutoDecodingPartsReplacement(BaseHandler):

    @no_parameter_check()
    async def get(self):
        cid = self.parameter.get("cid", "")
        vin = self.parameter.get("vin", "")
        vin_extra_info = self.parameter.get("vin_extra_info", '{}')
        if isinstance(vin_extra_info, str):
            vin_extra_info = json.loads(vin_extra_info)
        brandCode = self.parameter.get("brandCode", "")
        query_str = self.parameter.get("query_str", "")
        yc_id = self.parameter.get("yc_id", "")
        # merchant_id = self.parameter.get("merchant_id", "")
        is_log = self.parameter.get("is_log", "yes")

        logging.info(f"车架号=>{vin}, brandCode=>{brandCode}, cid=>{cid}, 译码名=>{query_str}, 译码员=>{yc_id}")

        decode_log = {
            "path": "auto_decoding_parts_replacement",
            "brandCode": brandCode,
            "vin": vin,
            "cid": cid,
            "query": query_str,
            "stdlabel": "",
            "pids": "",
            "is_show": "",
            "pid_length": "",
            "yc_id": yc_id,
            "create_time": datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        }

        query_str = self.label_process(query_str)

        try:
            alysis_list = sdk_info.match(query_str)
        except:
            alysis_list = ["", ""]

        stdlabel, position = alysis_list[0], alysis_list[1]

        if stdlabel:
            stdlabel, position = self.specail_pos_info_handler(stdlabel, position, query_str)

        else:
            decode_log['decode_msg'] = "未译码成功"
            if is_log == "yes":
                self._redis.lpush("auto_decoding_parts_log", json.dumps(decode_log))

            return ReturnData(CODE_600, [])

        if not brandCode or brandCode not in brand_db_map or not stdlabel:
            check_position_list = []
            return ReturnData(CODE_600, check_position_list)

        else:
            stdlabel = self.filter_str(stdlabel)

            if position:
                position_list = self.positionSplit(position)
            else:
                position_list = []

            parts_list = self.query_stdname_extra_info_with_cid(brandCode, cid, stdlabel)

            getPartList = []
            if "," in cid:
                for key, group in groupby(sorted(parts_list, key=itemgetter('cid')), itemgetter('cid')):
                    group = list(group)
                    for k, v in groupby(sorted(group, key=itemgetter('mid')), itemgetter('mid')):
                        getPartList.extend(remove_ship_brand(brandCode, list(v), vin_extra_info, vin, ctype="maintenance", _cid=key))

            else:
                for key, group in groupby(sorted(parts_list, key=itemgetter('mid')), itemgetter('mid')):
                    getPartList.extend(remove_ship_brand(brandCode, list(group), vin_extra_info, vin, ctype="maintenance", _cid=cid))

            colorvalue_list = {i.get("colorvalue") for i in getPartList if i.get('colorvalue') == 1}

            if not colorvalue_list and brandCode in ('audi', 'vwag', 'seat', 'skoda') and stdlabel in ('点火线圈', ' 火花塞', ' 保险杠', ' 减震器', ' 半轴', ' 大灯', ' 活性炭罐', ' 氧传感器', ' 外后视镜'):
                getPartList = remove_ship_brand(brandCode, parts_list, vin_extra_info, vin, ctype="parts", _cid=cid)

            check_position_list = self.checkPosition(getPartList, position_list)

            # if merchant_id != '3':
            check_position_list = self.brandManager(brandCode, check_position_list, position)

            is_show_pid = self.show_all_partinfo_status(check_position_list, stdlabel)

            if brandCode in ('audi', 'vwag', 'seat', 'skoda') and check_position_list and is_show_pid == 0:
                second_ship_list = [info for info in remove_ship_brand(brandCode, check_position_list, vin_extra_info, vin, ctype="strict", _cid=cid) if info.get("colorvalue", 1) == 1]
                if len(second_ship_list) == 1:
                    check_position_list = second_ship_list
                    is_show_pid = 1

            pid_length = len(check_position_list)

            pids = ",".join([i.get("pid") for i in check_position_list]) if check_position_list else ""

            logging.info(f"车架号=>{vin}, brandCode=>{brandCode}, cid=>{cid}, 标准名称=>{stdlabel}, 译码零件数量=>{pid_length}")

            decode_log.update({
                "stdlabel": stdlabel,
                "pids": pids,
                "is_show": is_show_pid,
                "pid_length": pid_length
            })
            if is_log == "yes":
                self._redis.lpush("auto_decoding_parts_log", json.dumps(decode_log))

            return ReturnData(CODE_1, check_position_list, is_show_pid=is_show_pid)

            # if merchant_id == '3':
            #     if pid_length == 1:
            #         return ReturnData(CODE_1, check_position_list, show_pid=1)
            #     else:
            #         return ReturnData(CODE_1, check_position_list, show_pid=0)
            #
            # else:
            #     return ReturnData(CODE_1, check_position_list, show_pid=is_show_pid)

    async def post(self):
        await self.get()

    # 是否将最后零件列表结果全部展示
    def show_all_partinfo_status(self, parts_list, stdlabel):
        position_weight_dict = {
            "前": 1,
            "后": 2,
            "左": 3,
            "右": 4,
            "内": 5,
            "外": 6,
            "上": 7,
            "下": 8,
            "中": 9
        }
        show_pidinfo_list = ['火花塞', '传动皮带']
        if len(parts_list) == 1:
            return 1

        if stdlabel in show_pidinfo_list:
            return 1

        remark_count_dict = {}
        for part in parts_list:
            standard_label_remark = part.get("standard_label_remark", "")
            if not standard_label_remark:
                standard_label_remark = ""
            else:
                standard_label_remark = ",".join(sorted(standard_label_remark.replace(" ", "").split(","), key=lambda x: position_weight_dict.get(x, 10)))

            if standard_label_remark in remark_count_dict:
                remark_count_dict[standard_label_remark] += 1
            else:
                remark_count_dict[standard_label_remark] = 1

        for remark_count in remark_count_dict.values():
            if remark_count > 1:
                return 0

        return 1

    # 针对于特殊的零件
    def specail_pos_info_handler(self, stdlabel, position, query_str):
        if stdlabel == "气门":
            if "进" in query_str:
                position = "进"
            elif "排" in query_str:
                position = "排"

        if stdlabel == "蓄电池":
            if "辅助" in query_str:
                position = "辅助"

        if stdlabel == "翼子板":
            if "后" in query_str:
                stdlabel = "侧车架"

        if stdlabel == '车标':
            if "中网" in query_str:
                position = "前"
            elif "举升门" in query_str or "行李箱盖" in query_str:
                position = "后"

        if stdlabel in ('前舱盖', '行李箱盖'):
            if "前" in query_str:
                stdlabel = "前舱盖"

        if stdlabel == "安全气囊":
            if "副驾驶" in query_str:
                position = "前右"

        if stdlabel == "安全带":
            if "副驾驶" in query_str:
                position = "前右"

        return stdlabel, position

    # 宝马品牌的特殊规则处理
    def bmw_series_handler(self, parts_list):
        if len(parts_list) == 1:
            return parts_list

        result_list = []
        for part_info in parts_list:
            label, standard_label = part_info.get("label", ""), part_info.get("standard_label", "")
            extra_info = part_info.get("extra_info")
            extra_info = json.loads(extra_info)
            ae_status = extra_info.get("AE", "")
            itid = part_info.get("itid", "")
            num = part_info.get("num", "")
            if ae_status == "AE" or itid == '--' or num == "03" or ("组件" in label and standard_label == '制动片'):
                continue
            result_list.append(part_info)

        if result_list:
            return result_list
        else:
            return parts_list

    # 奥迪品牌的特殊规则处理
    def audi_series_handler(self, parts_list):
        if len(parts_list) == 1:
            return parts_list

        result_list = []
        for part_info in parts_list:
            remark1 = part_info.get("remark1", "")
            condition = part_info.get("condition", "")
            if remark1:
                part_info['remark'] = remark1
            if remark1 == '1升':
                result_list.append(part_info)
            # if condition == 'L':
            #     result_list.append(part_info)

        if result_list:
            return result_list
        else:
            return parts_list

    # 路虎品牌的特殊规则处理
    def land_rover_series_handler(self, parts_list):
        if len(parts_list) == 1:
            return parts_list

        result_list = []
        for part_info in parts_list:
            pid = part_info.get("pid")
            standard_label = part_info.get("standard_label")

            if standard_label in ('变速箱油', '防冻液'):
                if pid in land_rover_oil_detail_info:
                    oil_info = land_rover_oil_detail_info[pid]
                else:
                    oil_info = ""

                if oil_info == '1L':
                    result_list.append(part_info)

            else:
                struct_extra_info = json.loads(part_info.get("struct_extra_info", "{}"))
                extra_info = json.loads(part_info.get("extra_info", "{}"))
                all_code_list = []
                if extra_info.get("filters", []):
                    for d in extra_info.get("filters", []):
                        all_code_list.extend(d.get("code", []))

                group_extra_info = struct_extra_info.get("group_extra_info", {})
                if group_extra_info and group_extra_info.get("filters", []):
                    for d in group_extra_info.get("filters", []):
                        all_code_list.extend(d.get("code", []))

                if struct_extra_info.get("filters", []):
                    for d in struct_extra_info.get("filters", []):
                        all_code_list.extend(d.get("code", []))

                if (21340 in all_code_list) or (19811 in all_code_list):
                    continue

                if part_info.get("pid", "") in land_rover_remachine_pid_list:
                    continue

                result_list.append(part_info)

        if result_list:
            return result_list
        else:
            return parts_list

    # 处理各个品牌相同规则过滤
    def all_series_handler(self, parts_list, position):
        if len(parts_list) == 1:
            return parts_list

        result_list = []
        for part_info in parts_list:
            standard_label, standard_label_remark = part_info.get("standard_label", ""), part_info.get("standard_label_remark", "")
            label, remark = part_info.get("label", ""), part_info.get("remark", "")
            verify_str = label + '##' + remark
            if standard_label == "蓄电池":
                if standard_label_remark == "辅助" and position == "辅助":
                    result_list.append(part_info)
                elif standard_label_remark == "辅助" and position == "":
                    continue
                elif standard_label_remark == "" and position == "":
                    result_list.append(part_info)

            elif "弃用零件" not in verify_str or "弃用" not in verify_str:
                result_list.append(part_info)

        if result_list:
            return result_list
        else:
            return parts_list

    # 根据不同的品牌处理零件(新增规则)
    def brandManager(self, brandCode, parts_list, position):

        parts_list = self.all_series_handler(parts_list, position)
        if len(parts_list) == 1:
            return parts_list

        if brandCode in ('audi', 'vwag', 'seat', 'skoda'):
            return self.audi_series_handler(parts_list)

        elif brandCode == 'bmw':
            return self.bmw_series_handler(parts_list)

        elif brandCode == 'land_rover':
            return self.land_rover_series_handler(parts_list)

        else:
            return parts_list

    def positionSplit(self, position):
        if "左" in position and '右' in position:
            position = position.replace("左", "").replace("右", "")
        if "前" in position and '后' in position:
            position = position.replace("前", "").replace("后", "")

        if position == "辅助":
            return ['辅助']
        else:
            return list(position)

    # 根据位置信息过滤出零件并检查颜色件(优先返回颜色件)
    def checkPosition(self, parts_list, position_list):
        result_list = []
        all_pid_list = []
        all_replacement_list = []

        for part_info in parts_list:
            if part_info.get('colorvalue', 1) == 0:
                continue
            standard_label_remark = part_info.get('standard_label_remark').replace(" ", "")
            standard_label_remark_list = standard_label_remark.split(",")
            pid = part_info.get("pid")
            if not standard_label_remark or set(standard_label_remark_list) >= set(position_list):
                if pid in all_pid_list:
                    continue

                pid_replacement = part_info.get("pid_replacement", [])
                if pid_replacement and pid in pid_replacement:
                    pid_replacement.remove(pid)
                    all_replacement_list.extend(pid_replacement)

                result_list.append(part_info)
                all_pid_list.append(pid)

        if all_replacement_list:
            replace_status = 0
            for pid_info in result_list:
                if pid_info.get("pid") in all_replacement_list:
                    replace_status += 1

            if len(result_list) == replace_status:
                result_list = [result_list[-1]]

        # logging.info("result_list: %s", result_list)
        colorparts_list = []
        for data in result_list:
            pid, parentpid = data.get("pid"), data.get("parentpid", "")
            if parentpid in all_pid_list:
                colorparts_list.append(data)

        if colorparts_list:
            return colorparts_list

        else:
            return result_list

    # 获取pid_filter_info_colleciton中的零件信息
    def query_stdname_extra_info_with_cid(self, brandCode, cid, query):
        """
            通过标准名称查询零件
        """
        db_name = brand_db_map[brandCode]
        if "," in cid:
            cid_list = cid.split(",")
            cond = {"cid": {"$in": cid_list}, "brandCode": brandCode}
        else:
            cond = {"cid": cid, 'brandCode': brandCode}

        cond['standard_label'] = query

        parts_list = list(self._mongo[db_name]['pid_filter_info_collection'].find(cond,
                                                                                  {'_id': 0,
                                                                                   'cid': 1,
                                                                                   'mid': 1,
                                                                                   'num': 1,
                                                                                   'imagePath': 1,
                                                                                   'pid': 1,
                                                                                   'pnum': 1,
                                                                                   'remark1': 1,
                                                                                   'brandCode': 1,
                                                                                   'subgroup': 1,
                                                                                   'struct_extra_info': 1,
                                                                                   'standard_label_remark': 1,
                                                                                   'extra_info': 1,
                                                                                   'remark': 1,
                                                                                   'condition': 1,
                                                                                   'struct_label': 1,
                                                                                   'itid': 1,
                                                                                   'label': 1,
                                                                                   'model': 1,
                                                                                   'standard_label': 1,
                                                                                   'quantity': 1,
                                                                                   'parentpid': 1,
                                                                                   'pid_replacement': 1,
                                                                                   'last_replace_pid': 1,
                                                                                   'last_l_replace_pid': 1}))

        return parts_list

    # 通过别名的名称搜索标准名称
    def alias_search_standard_label(self, query, size=200):
        query_dict = {
            "size": size,
            "query": {
                "bool": {
                    "must": [
                        {"term": {"alias_name": query}}
                    ]
                }
            }
        }

        return query_dict

    def delete_symbols(self, string):
        temp_list = []
        for letter in string:
            if u'\u4e00' <= letter <= u'\u9fff' or letter.isalpha() or letter.isdigit():
                temp_list.append(letter)
        return "".join(temp_list)

    def label_process(self, label):
        label = label.replace("\t", "").replace('"', "")
        label = label.replace("<br/>", "")
        label = self.delete_symbols(label)
        label = label.replace("，", "")
        label = label.upper()
        return label

    # 处理查询关键字中的特殊字符
    def filter_str(self, desstr, restr=''):
        res = re.compile("[^\\u4e00-\\u9fa5^a-z^A-Z]")
        f_str = res.sub(restr, desstr)
        f_str_upper = f_str.upper()
        if re.findall(r'[A-Z]', f_str):
            status = False
            for keyword in ['LED', 'EGR', 'HVAC', 'ABS', 'L', 'R', 'ACC', 'A柱', 'B柱', 'C柱', 'D柱']:
                if keyword in f_str_upper:
                    status = True

            if status:
                return f_str
            else:
                return re.sub(r'[a-zA-z]', '', f_str)

        else:
            return f_str


class AutoDecodingPartsMultiDataSource(BaseHandler):

    @no_parameter_check()
    async def get(self):
        cid = self.parameter.get("cid", "")
        vin = self.parameter.get("vin", "")
        vin = vin.upper()
        vin_extra_info = self.parameter.get("vin_extra_info", '{}')
        if isinstance(vin_extra_info, str):
            vin_extra_info = json.loads(vin_extra_info)
        brandCode = self.parameter.get("brandCode", "")
        src_query_str = self.parameter.get("query_str", "")
        yc_id = self.parameter.get("yc_id", "")
        # merchant_id = self.parameter.get("merchant_id", "")
        is_log = self.parameter.get("is_log", "yes")

        logging.info(f"车架号=>{vin}, brandCode=>{brandCode}, cid=>{cid}, 译码名=>{src_query_str}, 译码员=>{yc_id}")

        decode_log = {
            "path": "auto_decoding_parts_replacement",
            "brandCode": brandCode,
            "vin": vin,
            "cid": cid,
            "query": src_query_str,
            "stdlabel": "",
            "pids": "",
            "is_show": "",
            "pid_length": "",
            "yc_id": yc_id,
            "create_time": datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        }

        query_str = self.label_process(src_query_str)

        try:
            alysis_list = sdk_info.match(query_str)
        except:
            alysis_list = ["", ""]

        stdlabel, position = alysis_list[0], alysis_list[1]
        query_score = alysis_list[2]

        if stdlabel:
            stdlabel, position = self.specail_pos_info_handler(stdlabel, position, query_str)

        else:
            decode_log['decode_msg'] = "未译码成功"
            if is_log == "yes":
                self._redis.lpush("auto_decoding_parts_log", json.dumps(decode_log))

            return ReturnData(CODE_600, [])

        if not brandCode or brandCode not in brand_db_map or not stdlabel:
            check_position_list = []
            return ReturnData(CODE_600, check_position_list)

        else:
            stdlabel = self.filter_str(stdlabel)

            if position:
                position_list = self.positionSplit(position)
            else:
                position_list = []

            parts_list = self.query_stdname_extra_info_with_cid(brandCode, cid, stdlabel)

            getPartList = []
            if "," in cid:
                for key, group in groupby(sorted(parts_list, key=itemgetter('cid')), itemgetter('cid')):
                    group = list(group)
                    for k, v in groupby(sorted(group, key=itemgetter('mid')), itemgetter('mid')):
                        getPartList.extend(remove_ship_brand(brandCode, list(v), vin_extra_info, vin, ctype="maintenance", _cid=key))

            else:
                for key, group in groupby(sorted(parts_list, key=itemgetter('mid')), itemgetter('mid')):
                    getPartList.extend(remove_ship_brand(brandCode, list(group), vin_extra_info, vin, ctype="maintenance", _cid=cid))

            colorvalue_list = {i.get("colorvalue") for i in getPartList if i.get('colorvalue') == 1}

            if not colorvalue_list and brandCode in ('audi', 'vwag', 'seat', 'skoda') and stdlabel in ('点火线圈', ' 火花塞', ' 保险杠', ' 减震器', ' 半轴', ' 大灯', ' 活性炭罐', ' 氧传感器', ' 外后视镜'):
                getPartList = remove_ship_brand(brandCode, parts_list, vin_extra_info, vin, ctype="parts", _cid=cid)

            check_position_list = self.checkPosition(brandCode, vin, getPartList, position_list)

            # if merchant_id != '3':
            check_position_list = self.brandManager(brandCode, check_position_list, position)

            is_show_pid = self.show_all_partinfo_status(check_position_list, stdlabel)

            if brandCode in ('audi', 'vwag', 'seat', 'skoda') and check_position_list and is_show_pid == 0:
                second_ship_list = [info for info in remove_ship_brand(brandCode, check_position_list, vin_extra_info, vin, ctype="strict", _cid=cid) if info.get("colorvalue", 1) == 1]
                if len(second_ship_list) == 1:
                    check_position_list = second_ship_list
                    is_show_pid = 1

            pid_length = len(check_position_list)

            if query_score < 0.86 or src_query_str in zk_label_exists:
                check_position_list = []
                is_show_pid = 0

            cass_name = ""
            is_cass_decode = 0
            if is_show_pid == 0:
                # user_str = self.verify_user_query_str(src_query_str)
                cass_name_data = self._mongo['price']['zklabel_cass_name_mapping'].find_one({"zk_label": src_query_str})
                if cass_name_data:
                    cass_name = cass_name_data.get("cass_name")
                    cond = {"brandCode": brandCode,
                            "cid": cid}
                    if type(cass_name).__name__ == "str":
                        cond['cass_name'] = cass_name
                    else:
                        cond['cass_name'] = {"$in": cass_name}

                    pid_list = [i.get("pid") for i in self._mongo['price']['auto_decoding_user_data_zk'].find(cond, {"pid": 1})]

                    if pid_list:
                        user_part_list = []
                        pid_info_list = self.query_stdname_extra_info_with_cid(brandCode, cid, pid_list, status='pid')
                        for key, group in groupby(sorted(pid_info_list, key=itemgetter('mid')), itemgetter('mid')):
                            user_part_list.extend(remove_ship_brand(brandCode, list(group), vin_extra_info, vin, ctype="maintenance", _cid=cid))

                    else:
                        user_part_list = []

                    if user_part_list:
                        check_position_list = self.checkPosition_v2(brandCode, vin, user_part_list)
                        is_cass_decode = 1
                        if check_position_list:
                            is_show_pid = 1

            pids = ",".join([i.get("pid") for i in check_position_list]) if check_position_list else ""

            logging.info(f"车架号=>{vin}, brandCode=>{brandCode}, cid=>{cid}, 标准名称=>{stdlabel}, 译码零件数量=>{pid_length}")

            decode_log.update({
                "stdlabel": stdlabel,
                "pids": pids,
                "is_show": is_show_pid,
                "pid_length": pid_length
            })
            if is_log == "yes":
                self._redis.lpush("auto_decoding_parts_log", json.dumps(decode_log))

            return ReturnData(CODE_1, check_position_list,
                              is_show_pid=is_show_pid,
                              stdlabel=stdlabel,
                              cass_name=cass_name,
                              is_cass_decode=is_cass_decode)

            # if merchant_id == '3':
            #     if pid_length == 1:
            #         return ReturnData(CODE_1, check_position_list, show_pid=1)
            #     else:
            #         return ReturnData(CODE_1, check_position_list, show_pid=0)
            #
            # else:
            #     return ReturnData(CODE_1, check_position_list, show_pid=is_show_pid)

    async def post(self):
        await self.get()

    def checkPosition_v2(self, brandCode, vin, parts_list):
        result_list = []
        all_pid_list = []
        all_replacement_list = []

        for part_info in parts_list:
            if part_info.get('colorvalue', 1) == 0:
                continue

            pid = part_info.get("pid")
            if pid in all_pid_list:
                continue

            if brandCode in ('audi', 'vwag', 'skoda', 'seat'):
                if vin.startswith("L") and "last_l_replace_pid" in part_info:
                    part_info["last_replace_pid"] = part_info["last_l_replace_pid"]

            pid_replacement = part_info.get("pid_replacement", [])
            if pid_replacement and pid in pid_replacement:
                pid_replacement.remove(pid)
                all_replacement_list.extend(pid_replacement)

            result_list.append(part_info)
            all_pid_list.append(pid)

        if all_replacement_list:
            replace_status = 0
            for pid_info in result_list:
                if pid_info.get("pid") in all_replacement_list:
                    replace_status += 1

            if len(result_list) == replace_status:
                result_list = [result_list[-1]]

        # logging.info("result_list: %s", result_list)
        colorparts_list = []
        for data in result_list:
            pid, parentpid = data.get("pid"), data.get("parentpid", "")
            if parentpid in all_pid_list:
                colorparts_list.append(data)

        if colorparts_list:
            return colorparts_list

        else:
            return result_list

    # 是否将最后零件列表结果全部展示
    def show_all_partinfo_status(self, parts_list, stdlabel):
        position_weight_dict = {
            "前": 1,
            "后": 2,
            "左": 3,
            "右": 4,
            "内": 5,
            "外": 6,
            "上": 7,
            "下": 8,
            "中": 9
        }
        if not parts_list:
            return 0

        show_pidinfo_list = ['火花塞', '传动皮带']
        if len(parts_list) == 1:
            return 1

        if stdlabel in show_pidinfo_list:
            return 1

        remark_count_dict = {}
        for part in parts_list:
            standard_label_remark = part.get("standard_label_remark", "")
            if not standard_label_remark:
                standard_label_remark = ""
            else:
                standard_label_remark = ",".join(sorted(standard_label_remark.replace(" ", "").split(","), key=lambda x: position_weight_dict.get(x, 10)))

            if standard_label_remark in remark_count_dict:
                remark_count_dict[standard_label_remark] += 1
            else:
                remark_count_dict[standard_label_remark] = 1

        for remark_count in remark_count_dict.values():
            if remark_count > 1:
                return 0

        return 1

    # 针对于特殊的零件
    def specail_pos_info_handler(self, stdlabel, position, query_str):
        if stdlabel == "气门":
            if "进" in query_str:
                position = "进"
            elif "排" in query_str:
                position = "排"

        if stdlabel == "蓄电池":
            if "辅助" in query_str:
                position = "辅助"

        if stdlabel == "翼子板":
            if "后" in query_str:
                stdlabel = "侧围外板"

        if stdlabel == '车标':
            if "中网" in query_str:
                position = "前"
            elif "举升门" in query_str or "行李箱盖" in query_str:
                position = "后"

        if stdlabel == '挡风玻璃':
            if '后' in query_str:
                stdlabel = '后窗玻璃'

        if stdlabel in ('前舱盖', '行李箱盖'):
            if "前" in query_str:
                stdlabel = "前舱盖"

        if stdlabel == "安全气囊":
            if "副驾驶" in query_str:
                position = "前右"

        if stdlabel == "安全带":
            if "副驾驶" in query_str:
                position = "前右"

        return stdlabel, position

    # 宝马品牌的特殊规则处理
    def bmw_series_handler(self, parts_list):
        if len(parts_list) == 1:
            return parts_list

        result_list = []
        for part_info in parts_list:
            label, standard_label = part_info.get("label", ""), part_info.get("standard_label", "")
            extra_info = part_info.get("extra_info")
            extra_info = json.loads(extra_info)
            ae_status = extra_info.get("AE", "")
            itid = part_info.get("itid", "")
            num = part_info.get("num", "")
            if ae_status == "AE" or itid == '--' or num == "03" or ("组件" in label and standard_label == '制动片'):
                continue
            result_list.append(part_info)

        if result_list:
            return result_list
        else:
            return parts_list

    # 奥迪品牌的特殊规则处理
    def audi_series_handler(self, parts_list):
        if len(parts_list) == 1:
            return parts_list

        result_list = []
        for part_info in parts_list:
            remark1 = part_info.get("remark1", "")
            condition = part_info.get("condition", "")
            if remark1:
                part_info['remark'] = remark1
            if remark1 == '1升':
                result_list.append(part_info)
            # if condition == 'L':
            #     result_list.append(part_info)

        if result_list:
            return result_list
        else:
            return parts_list

    # 路虎品牌的特殊规则处理
    def land_rover_series_handler(self, parts_list):
        if len(parts_list) == 1:
            return parts_list

        result_list = []
        for part_info in parts_list:
            pid = part_info.get("pid")
            standard_label = part_info.get("standard_label")

            if standard_label in ('变速箱油', '防冻液'):
                if pid in land_rover_oil_detail_info:
                    oil_info = land_rover_oil_detail_info[pid]
                else:
                    oil_info = ""

                if oil_info == '1L':
                    result_list.append(part_info)

            else:
                struct_extra_info = json.loads(part_info.get("struct_extra_info", "{}"))
                extra_info = json.loads(part_info.get("extra_info", "{}"))
                all_code_list = []
                if extra_info.get("filters", []):
                    for d in extra_info.get("filters", []):
                        all_code_list.extend(d.get("code", []))

                group_extra_info = struct_extra_info.get("group_extra_info", {})
                if group_extra_info and group_extra_info.get("filters", []):
                    for d in group_extra_info.get("filters", []):
                        all_code_list.extend(d.get("code", []))

                if struct_extra_info.get("filters", []):
                    for d in struct_extra_info.get("filters", []):
                        all_code_list.extend(d.get("code", []))

                if (21340 in all_code_list) or (19811 in all_code_list):
                    continue

                if part_info.get("pid", "") in land_rover_remachine_pid_list:
                    continue

                result_list.append(part_info)

        if result_list:
            return result_list
        else:
            return parts_list

    # 处理各个品牌相同规则过滤
    def all_series_handler(self, parts_list, position):
        if len(parts_list) == 1:
            return parts_list

        result_list = []
        for part_info in parts_list:
            standard_label, standard_label_remark = part_info.get("standard_label", ""), part_info.get("standard_label_remark", "")
            label, remark = part_info.get("label", ""), part_info.get("remark", "")
            verify_str = label + '##' + remark
            if standard_label == "蓄电池":
                if standard_label_remark == "辅助" and position == "辅助":
                    result_list.append(part_info)
                elif standard_label_remark == "辅助" and position == "":
                    continue
                elif standard_label_remark == "" and position == "":
                    result_list.append(part_info)

            elif "弃用零件" not in verify_str or "弃用" not in verify_str:
                result_list.append(part_info)

        if result_list:
            return result_list
        else:
            return parts_list

    # 根据不同的品牌处理零件(新增规则)
    def brandManager(self, brandCode, parts_list, position):

        parts_list = self.all_series_handler(parts_list, position)
        if len(parts_list) == 1:
            return parts_list

        if brandCode in ('audi', 'vwag', 'seat', 'skoda'):
            return self.audi_series_handler(parts_list)

        elif brandCode == 'bmw':
            return self.bmw_series_handler(parts_list)

        elif brandCode == 'land_rover':
            return self.land_rover_series_handler(parts_list)

        else:
            return parts_list

    def positionSplit(self, position):
        if "左" in position and '右' in position:
            position = position.replace("左", "").replace("右", "")
        if "前" in position and '后' in position:
            position = position.replace("前", "").replace("后", "")

        if position == "辅助":
            return ['辅助']
        else:
            return list(position)

    # 根据位置信息过滤出零件并检查颜色件(优先返回颜色件)
    def checkPosition(self, brandCode, vin, parts_list, position_list):
        result_list = []
        all_pid_list = []
        all_replacement_list = []

        for part_info in parts_list:
            if part_info.get('colorvalue', 1) == 0:
                continue
            standard_label_remark = part_info.get('standard_label_remark').replace(" ", "")
            standard_label_remark_list = standard_label_remark.split(",")
            pid = part_info.get("pid")

            if brandCode in ('audi', 'vwag', 'skoda', 'seat'):
                if vin.startswith("L") and "last_l_replace_pid" in part_info:
                    part_info["last_replace_pid"] = part_info["last_l_replace_pid"]

            if not standard_label_remark or set(standard_label_remark_list) >= set(position_list):
                if pid in all_pid_list:
                    continue

                pid_replacement = part_info.get("pid_replacement", [])
                if pid_replacement and pid in pid_replacement:
                    pid_replacement.remove(pid)
                    all_replacement_list.extend(pid_replacement)

                result_list.append(part_info)
                all_pid_list.append(pid)

        if all_replacement_list:
            replace_status = 0
            for pid_info in result_list:
                if pid_info.get("pid") in all_replacement_list:
                    replace_status += 1

            if len(result_list) == replace_status:
                result_list = [result_list[-1]]

        # logging.info("result_list: %s", result_list)
        colorparts_list = []
        for data in result_list:
            pid, parentpid = data.get("pid"), data.get("parentpid", "")
            if parentpid in all_pid_list:
                colorparts_list.append(data)

        if colorparts_list:
            return colorparts_list

        else:
            return result_list

    # 获取pid_filter_info_colleciton中的零件信息
    def query_stdname_extra_info_with_cid(self, brandCode, cid, query, status='name'):
        """
            通过标准名称查询零件
        """
        db_name = brand_db_map[brandCode]
        if "," in cid:
            cid_list = cid.split(",")
            cond = {"cid": {"$in": cid_list}, "brandCode": brandCode}
        else:
            cond = {"cid": cid, 'brandCode': brandCode}

        if status == 'name':
            cond['standard_label'] = query
        else:
            cond['pid'] = {"$in": query}

        parts_list = list(self._mongo[db_name]['pid_filter_info_collection'].find(cond,
                                                                                  {'_id': 0,
                                                                                   'cid': 1,
                                                                                   'mid': 1,
                                                                                   'num': 1,
                                                                                   'imagePath': 1,
                                                                                   'pid': 1,
                                                                                   'pnum': 1,
                                                                                   'remark1': 1,
                                                                                   'brandCode': 1,
                                                                                   'subgroup': 1,
                                                                                   'struct_extra_info': 1,
                                                                                   'standard_label_remark': 1,
                                                                                   'extra_info': 1,
                                                                                   'remark': 1,
                                                                                   'condition': 1,
                                                                                   'struct_label': 1,
                                                                                   'itid': 1,
                                                                                   'label': 1,
                                                                                   'model': 1,
                                                                                   'standard_label': 1,
                                                                                   'quantity': 1,
                                                                                   'parentpid': 1,
                                                                                   'pid_replacement': 1,
                                                                                   'last_replace_pid': 1,
                                                                                   'last_l_replace_pid': 1}))

        return parts_list

    # 通过别名的名称搜索标准名称
    def alias_search_standard_label(self, query, size=200):
        query_dict = {
            "size": size,
            "query": {
                "bool": {
                    "must": [
                        {"term": {"alias_name": query}}
                    ]
                }
            }
        }

        return query_dict

    def delete_symbols(self, string):
        temp_list = []
        for letter in string:
            if u'\u4e00' <= letter <= u'\u9fff' or letter.isalpha() or letter.isdigit():
                temp_list.append(letter)
        return "".join(temp_list)

    def label_process(self, label):
        label = label.replace("\t", "").replace('"', "")
        label = label.replace("<br/>", "")
        label = self.delete_symbols(label)
        label = label.replace("，", "")
        label = label.upper()
        return label

    # 处理查询关键字中的特殊字符
    def filter_str(self, desstr, restr=''):
        res = re.compile("[^\\u4e00-\\u9fa5^a-z^A-Z]")
        f_str = res.sub(restr, desstr)
        f_str_upper = f_str.upper()
        if re.findall(r'[A-Z]', f_str):
            status = False
            for keyword in ['LED', 'EGR', 'HVAC', 'ABS', 'L', 'R', 'ACC', 'A柱', 'B柱', 'C柱', 'D柱']:
                if keyword in f_str_upper:
                    status = True

            if status:
                return f_str
            else:
                return re.sub(r'[a-zA-z]', '', f_str)

        else:
            return f_str

    # 通过北航的检索接口查询用户搜索的名称
    def verify_user_query_str(self, query_str):
        http_str = "https://nlpapi.actkg.com/relevant"
        params = {"query": query_str}
        req = requests.get(http_str, params=params)

        result_str = ""
        for r in req.json().get('data', []):
            verify_label, similar = r
            if similar == 1 and verify_label == query_str:
                result_str = verify_label

        return result_str
