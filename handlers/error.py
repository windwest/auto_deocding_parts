#!/usr/bin/env python
# -*- coding: utf-8 -*-

# @Time    : 2020/9/15 下午2:17
# @Author  : Hanley
# @File    : error.py
# @Desc    : 

from commons.status import *
from commons.wrapper import (
    no_parameter_check
)
from utils.request_util import BaseHandler, ReturnData


class ErrorHandler(BaseHandler):

    @no_parameter_check()
    async def post(self, *args, **kwargs):
        return ReturnData(CODE_404)

    async def get(self, *args, **kwargs):
        await self.post(*args, **kwargs)