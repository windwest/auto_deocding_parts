#!/usr/bin/env python
# -*- coding: utf-8 -*-

# @Time    : 2020/9/15 下午1:57
# @Author  : Hanley
# @File    : user.py
# @Desc    : 

import datetime
import time

from playhouse.shortcuts import model_to_dict

from commons.common import Common, DealEncrypt
from commons.constant import Constant, RedisKey
from commons.scheme import Scheme
from commons.initlog import logging
from commons.status import *
from commons.wrapper import (
    parameter_check,
    no_parameter_check,
)
from models.user import User


from utils.request_util import BaseHandler, ReturnData


class UserLogin(BaseHandler):
    """
    用户登录
    redis_key:domain+channel+uid
    """

    @no_parameter_check()
    async def post(self):
        mobile = self.parameter["mobile"].replace(" ", "")
        sms_code = self.parameter.get("sms_code")
        password = self.parameter.get("password", "").replace(" ", "")
        device = self.parameter["device"]
        channel = self.parameter["channel"]
        domain = self.parameter["domain"]
        # 校验手机号
        if not Common.validate_phone(mobile):
            return ReturnData(CODE_201)
        # 校验设备
        if device not in Constant.DEVICE:
            return ReturnData(CODE_405)
        user_account = User.get_or_none(User.mobile == mobile)
        # 账号不存在
        if user_account is None:
            return ReturnData(CODE_203)
        # 账号已禁用
        if user_account.status != 1:
            return ReturnData(CODE_210)
        # 密码错误
        if user_account.password != password:
            return ReturnData(CODE_203)
        # 正确返回
        data = {}
        now = time.strftime("%Y-%m-%d %X")
        dict_update = {
            "device": device,
            "last_login_time": now,
            "last_login_ip": self.parameter.get("real_ip")
        }
        User.update(dict_update).where(
            (User.uid == user_account.uid) &
            ((User.device != device) |
             (User.last_login_time != now))
        ).execute()
        data["uid"] = user_account.uid
        data["role_id"] = user_account.role_id
        salt = Common.generate_uuid()
        data["token"] = self.set_token(domain, channel, salt, user_account.uid)
        self.set_author_cookie(
            domain, channel, device, data["uid"], data["token"])
        return ReturnData(CODE_1, data)

    async def get(self):
        await self.post()

    def set_token(self, domain, channel, salt, uid):
        token = DealEncrypt.hash_sha256_encrypt(salt)
        login_key = "".join([domain, channel, uid])
        self._redis.set(login_key, token, ex=RedisKey.LOGIN_EXPIRE)
        return token

    def set_author_cookie(self, domain, channel, device, uid, token):
        real_ip = self.request.headers.get("X-Real-IP")
        real_ip = real_ip if real_ip else self.request.remote_ip
        preFix = domain.split(".")[0]
        channelCookieKey = Constant.COOKIE_CHANNEL.format(preFix)
        deviceCookieKey = Constant.COOKIE_DEVICE.format(preFix)
        uidCookieKey = Constant.COOKIE_UID.format(preFix)
        tokenCookieKey = Constant.COOKIE_TOKEN.format(preFix)
        self.clear_cookie("real_ip")
        self.clear_cookie(channelCookieKey)
        self.clear_cookie(deviceCookieKey)
        self.clear_cookie(uidCookieKey)
        self.clear_cookie(tokenCookieKey)

        self.set_secure_cookie("real_ip", real_ip)
        self.set_secure_cookie(deviceCookieKey, device)
        self.set_secure_cookie(channelCookieKey, channel)
        self.set_secure_cookie(uidCookieKey, uid)
        self.set_secure_cookie(tokenCookieKey, token)