### 项目启动命令
env可选项dev，prod，local
env默认dev
port默认14375
cd $project;python3 server.py --env=$env --port=$port

### 项目更新命令
cd $project;git pull

### 异步队列启动命令(需要先启动后端服务，生成配置文件)
cd $project;celery -B -A async_task.app worker -l info -s celerybeat-schedule
