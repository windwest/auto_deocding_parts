#!/usr/bin/env python
# -*- coding: utf-8 -*-

# @Time    : 2020/9/15 上午10:52
# @Author  : Hanley
# @File    : status.py
# @Desc    : 

"""
用户相关：2**
操作相关：4**
程序相关：5**
"""

ENV = None

CODE_0 = 0
CODE_1 = 1
CODE_201 = 201
CODE_203 = 203
CODE_204 = 204
CODE_210 = 210
CODE_404 = 404
CODE_405 = 405
CODE_500 = 500
CODE_503 = 503
CODE_600 = 600
