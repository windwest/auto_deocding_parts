#coding: utf-8
from utils.database_util import MongodbConnect

mongo_conn = MongodbConnect().client

land_rover_remachine_pid_list = [part_info.get("pid") for part_info in
                                 mongo_conn['price']['auto_decoding_parts_detail_info'].find(
                                     {"brandCode": "land_rover", "status": "更换件(重新加工)"})]

land_rover_oil_detail_info = {part_info.get("pid"): part_info.get("status") for part_info in
                              mongo_conn['price']['auto_decoding_parts_detail_info'].find(
                                  {"brandCode": "land_rover", "standard_label": {"$in": ['防冻液', '变速箱油']}})}
