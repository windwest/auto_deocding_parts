#!/usr/bin/env python
# -*- coding: utf-8 -*-

# @Time    : 2020/9/15 上午10:12
# @Author  : Hanley
# @File    : constant.py
# @Desc    : 


import os


def make_file_path(config_name: str) -> str:
    """
    项目根目录路径拼接
    :param config_name:
    :return:
    """
    curr_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    return os.path.join(curr_dir, config_name)


class RedisKey:
    """
    缓存key-expire
    """
    LOGIN_EXPIRE = 60 * 60 * 24 * 30
    pass


class Constant:
    """
    项目常量
    """

    INI_PATH = make_file_path('config.ini')

    # encrypt config
    BLOCK_SIZE = 16
    # base64.encodebytes(get_random_bytes(16)).strip().decode()
    ENCRYPT_KEY = 'PSznaRJ6tNR9D8kcCxtH0A=='

    APP_DEVICE = {"ios", "android"}
    WEB_DEVICE = {"web"}
    DEVICE = set()
    DEVICE.update(APP_DEVICE)
    DEVICE.update(WEB_DEVICE)

    COOKIE_CHANNEL = "{}_007_channel"
    COOKIE_DEVICE = "{}_007_device"
    COOKIE_UID = "{}_007_uid"
    COOKIE_TOKEN = "{}_007_token"


class ReturnCodeMap:

    CN_CODE = {
        0: "错误返回",
        1: "成功返回",
        201: "手机号格式错误",
        203: "账户不存在，请先注册",
        204: "登陆密码错误，请输入正确密码",
        210: "该账户异常",
        404: "未知路径",
        405: "未知路径",
        500: "服务器出小差了",
        503: "外部接口调用异常",
        600: "传入参数错误"
    }


if __name__ == '__main__':
    pass
